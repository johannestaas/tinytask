import shutil
import traceback

from uuid import UUID
from functools import wraps
from tempfile import mkdtemp
from multiprocessing import Process, Pipe

from .task import Task, TaskStatus
from .protocol import Command, Request, Response, SerializedTask
from .exceptions import TaskNotFoundError, SchedulerConfigError


def _task_or_die(func):
    '''
    For the Scheduler instance methods, discovers whether the first argument is
    a ``Task`` instance, or a string guid, and ensures the task exists first.
    If it doesn't exist, it raises ``TaskNotFoundError``.
    If it does, it invokes the decorated function with the task as the first
    argument.
    '''

    @wraps(func)
    def wrapped(self, task_or_guid, *args, **kwargs):
        if isinstance(task_or_guid, UUID):
            task_or_guid = str(task_or_guid)
        if isinstance(task_or_guid, str):
            guid = task_or_guid
            if guid not in self.tasks:
                raise TaskNotFoundError(
                    'cant find task with guid {}'.format(guid)
                )
            task = self.tasks[guid]
        else:
            task = task_or_guid
        return func(self, task, *args, **kwargs)

    return wrapped


class Scheduler:
    RESULT_DIR = mkdtemp(prefix='tinytask_')
    VALID_SERIALIZERS = {'json', 'pickle'}

    def __repr__(self):
        return (
            'Scheduler(concurrency={self.concurrency!r}, '
            '#tasks={len(self.tasks)}, #running={len(self.running)}, '
            '#waiting={len(self.waiting)}, '
            'child={self.child and self.child.pid!r})'
        ).format(self=self)

    def __init__(self, concurrency=None, serializer='json'):
        self.concurrency = concurrency
        self.tasks = {}
        self.completed = {}
        self.running = {}
        self.waiting = {}
        self.waiting_tasks = []
        self.pipe = None
        self.scheduler_running = True
        self.child = None
        if serializer not in Scheduler.VALID_SERIALIZERS:
            raise SchedulerConfigError(
                f'serializer must be one of json/pickle, not: {serializer!r}'
            )
        self.serializer = serializer

    def _run(self, pipe):
        self.pipe = pipe
        while self.scheduler_running:
            try:
                data = self._listen()
            except TaskNotFoundError as e:
                self.pipe.send(Response.not_found(str(e)))
            except Exception:
                self.pipe.send(Response.error(traceback.format_exc()))
            else:
                # If pipe is closed or no message, don't send anything.
                if data is not None:
                    self.pipe.send(Response.success(data))

    def _poll(self):
        try:
            # Poll for one second, then just refresh tasks and start waiting
            # again.
            if not self.pipe.poll(1):
                return False
        except EOFError:
            self._quit()
            return False
        except KeyboardInterrupt:
            print('ctrl-c received')
            self._quit()
            return False
        return True

    def _listen(self):
        self._refresh_tasks()
        self._start_waiting()
        # Get next command.
        if not self._poll():
            # If pipe is closed or ctrl-c, quit.
            return None
        request = self._recv()
        if request is None:
            # If pipe is closed or ctrl-c...
            return None
        if request.command == Command.quit:
            # Scheduler should quit.
            return self._quit()
        task = self._task_from_request(request)
        # Check if it completed.
        if task.guid in self.running:
            self._refresh_task(task)
        if request.command == Command.kill:
            return self._kill_task(task)
        elif request.command == Command.start:
            if (
                self.concurrency is None or
                len(self.running) < self.concurrency
            ):
                return self._start_task(
                    task, args=request.args, kwargs=request.kwargs,
                )
            else:
                return self._wait_task(
                    task, args=request.args, kwargs=request.kwargs,
                )
        elif request.command == Command.get:
            return self._get(task)

    @_task_or_die
    def _refresh_task(self, task):
        if (
            task.guid in self.running and
            task.status in TaskStatus.done()
        ):
            self._complete_task(task)

    def _refresh_tasks(self):
        for task in list(self.running.values()):
            self._refresh_task(task)

    def _get_task(self, guid):
        if guid not in self.tasks:
            raise TaskNotFoundError('missing task with guid {}'.format(guid))
        return self.tasks[guid]

    def _task_from_request(self, request):
        if request.guid is not None:
            return self._get_task(request.guid)
        if request.function is not None:
            task = Task(request.function, Scheduler.RESULT_DIR)
            self.tasks[task.guid] = task
            return task
        return None

    def _start_waiting(self):
        if self.concurrency is None:
            # None should be waiting.
            return
        while len(self.running) < self.concurrency and self.waiting_tasks:
            # Start from FIFO list.
            task, args, kwargs = self.waiting_tasks[0]
            self._start_task(task, args, kwargs)
            self.waiting_tasks = self.waiting_tasks[1:]
            del self.waiting[task.guid]

    def _start_task(self, task, args=None, kwargs=None):
        task.start(args=args, kwargs=kwargs)
        self.running[task.guid] = task
        return task.guid

    def _wait_task(self, task, args=None, kwargs=None):
        self.waiting_tasks.append((task, args or (), kwargs or {}))
        self.waiting[task.guid] = task
        return task.guid

    def _quit(self):
        self.scheduler_running = False
        for task in self.running.values():
            if task.running:
                print('killing pid {}'.format(task.pid))
                task.kill()
        return True

    @_task_or_die
    def _kill_task(self, task):
        task.kill()
        self._complete_task(task)
        return task.guid

    @_task_or_die
    def _complete_task(self, task):
        if task.guid in self.running:
            del self.running[task.guid]
        if task.guid not in self.completed:
            self.completed[task.guid] = task

    @_task_or_die
    def _get(self, task):
        return SerializedTask.deserialize(task.serialize())

    def _recv(self):
        try:
            return self.pipe.recv()
        except KeyboardInterrupt:
            print('ctrl-c received')
            self._quit()
            return None
        except EOFError:
            print('pipe closed')
            self._quit()
            return None

    def get(self, guid):
        '''
        Returns the Task from the forked scheduler.

        :param Task|guid task_or_guid: a ``Task`` instance or string guid.
        :return: a ``Task`` object.
        '''
        self.pipe.send(Request(Command.get, guid=guid))
        return self._recv()

    def kill(self, guid):
        self.pipe.send(Request(Command.kill, guid=guid))
        return self._recv()

    def s(self, function):
        def _starter(*args, **kwargs):
            return self.schedule(function, args=args, kwargs=kwargs)
        return _starter

    def schedule(self, function, args=None, kwargs=None):
        self.pipe.send(
            Request(Command.start, function=function, args=args, kwargs=kwargs)
        )
        return self._recv()

    def quit(self, delete_results=True):
        if delete_results:
            shutil.rmtree(Scheduler.RESULT_DIR)
        self.pipe.send(Request(Command.quit))
        return self._recv()

    def run(self):
        # Create a pipe where the parent has the parent end, and the child end
        # gets passed to the child which sets up the pipe as that.
        self.pipe, child_end = Pipe()
        self.child = Process(target=self._run, args=(child_end,))
        self.child.start()
