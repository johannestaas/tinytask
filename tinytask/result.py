import os
import json
from enum import Enum
from datetime import datetime


class ResultType(Enum):
    # Successfully finished.
    success = 'success'
    # If wrapped function errored out.
    failed = 'failed'
    # If the result file is missing.
    not_found = 'not_found'
    # need timeout?


class Result:
    __slots__ = ('result_type', 'data', 'started', 'finished')

    def __repr__(self):
        result_type = self.result_type.value
        data = self.data
        started = self.started and self.started.isoformat()
        finished = self.finished and self.finished.isoformat()
        return (
            'Result({result_type}, data={data!r}, started={started!r}, '
            'finished={finished!r})'
        ).format(
            result_type=result_type, data=data, started=started,
            finished=finished,
        )

    def __init__(self, result_type, data, started, finished):
        self.result_type = result_type
        self.data = data
        self.started = started
        self.finished = finished

    def write(self, path):
        lock = path + '.lock'
        with open(lock, 'w') as f:
            json.dump(self.serialize(), f)
        os.rename(lock, path)
        return path

    def serialize(self):
        # If not_found, it's None.
        started = self.started and self.started.timestamp()
        finished = self.finished and self.finished.timestamp()
        return {
            'type': self.result_type.value,
            'data': self.data,
            'started': started,
            'finished': finished,
        }

    @classmethod
    def deserialize(cls, serial):
        result_type = ResultType[serial['type']]
        data = serial['data']
        started, finished = serial['started'], serial['finished']
        started = started and datetime.fromtimestamp(started)
        finished = finished and datetime.fromtimestamp(finished)
        return cls(result_type, data, started, finished)

    @classmethod
    def get(cls, path):
        if not os.path.exists(path):
            return cls(ResultType.not_found, None, None, None)
        with open(path) as f:
            serial = json.load(f)
        return cls.deserialize(serial)

    @classmethod
    def failed(cls, error, started, finished):
        return cls(ResultType.failed, error, started, finished)

    @classmethod
    def success(cls, data, started, finished):
        # Should be a task, but could be anything that serializes.
        if callable(getattr(data, 'serialize', None)):
            data = data.serialize()
        return cls(ResultType.success, data, started, finished)
