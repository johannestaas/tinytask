import os
import traceback
from enum import Enum
from uuid import uuid4
from functools import wraps
from datetime import datetime
from multiprocessing import Process

from .result import Result, ResultType
from .exceptions import TaskStartedError


class TaskStatus(Enum):
    created = 'created'
    running = 'running'
    killed = 'killed'
    complete = 'complete'
    failed = 'failed'

    @classmethod
    def done(cls):
        return {cls.killed, cls.complete, cls.failed}


class Task:
    __slots__ = ('guid', 'proc', 'function', '_status', '_result_dir')

    def __repr__(self):
        proc_pid = self.proc and self.proc.pid
        func_name = self.function and self.function.__qualname__
        return (
            'Task({self.guid}, proc={proc_pid!r}, '
            'function={func_name}, '
            'status={self._status.value})'
        ).format(self=self, proc_pid=proc_pid, func_name=func_name)

    def __init__(self, function, result_dir):
        self.guid = str(uuid4())
        self.proc = None
        self.function = function
        self._status = TaskStatus.created
        self._result_dir = result_dir

    def _refresh_status(self):
        if self._status in TaskStatus.done():
            return
        if self.proc is None:
            self._status = TaskStatus.created
        elif self.proc.is_alive():
            self._status = TaskStatus.running
        else:
            result_type = self.result.result_type
            if result_type == ResultType.success:
                self._status = TaskStatus.complete
            else:
                # Could be not_found too...
                self._status = TaskStatus.failed

    @property
    def status(self):
        self._refresh_status()
        return self._status

    @property
    def running(self):
        return self.status == TaskStatus.running

    @property
    def pid(self):
        return self.proc and self.proc.pid

    @property
    def exitcode(self):
        return self.proc and self.proc.exitcode

    @property
    def result(self):
        return Result.get(self.path)

    @property
    def path(self):
        return os.path.join(self._result_dir, self.guid)

    def serialize(self):
        return {
            'guid': self.guid,
            'status': self.status.value,
            'running': self.running,
            'done': self.status in TaskStatus.done(),
            'pid': self.pid,
            'exitcode': self.exitcode,
            'result': self.result,
        }

    def start(self, args=None, kwargs=None):
        args = args or ()
        kwargs = kwargs or {}
        if self.proc is not None:
            raise TaskStartedError('already started task with guid {}'.format(
                self.guid
            ))
        target = self._wrapped_function()
        self.proc = Process(target=target, args=args, kwargs=kwargs)
        self.proc.start()

    def kill(self):
        if self.running:
            self._status = TaskStatus.killed
            return self.proc.terminate()

    def _wrapped_function(self):

        @wraps(self.function)
        def _function(*args, **kwargs):
            started = datetime.now()
            data = None
            error = None
            try:
                data = self.function(*args, **kwargs)
            except Exception:
                error = traceback.format_exc()
            finally:
                finished = datetime.now()
            if error is not None:
                result = Result.failed(error, started, finished)
            else:
                result = Result.success(data, started, finished)
            result.write(self.path)
            return result

        return _function
