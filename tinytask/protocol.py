from enum import Enum
from uuid import UUID


class Command(Enum):
    quit = 0
    kill = 1
    start = 2
    get = 3


class Request:
    __slots__ = ('command', 'guid', 'function', 'args', 'kwargs')

    def __repr__(self):
        return (
            'Request({self.command.value}, {self.guid}, '
            '{self.function.__qualname__}, {self.args!r}, {self.kwargs!r})'
        ).format(self=self)

    def __init__(
        self, command, guid=None, function=None, args=None, kwargs=None,
    ):
        self.command = command
        self.guid = guid
        self.function = function
        self.args = args or ()
        self.kwargs = kwargs or {}


class ResponseType(Enum):
    success = 'success'
    error = 'error'
    not_found = 'not_found'


class Response:
    __slots__ = ('response_type', 'msg', 'data')

    def __repr__(self):
        return (
            'Response({self.response_type.value}, msg={self.msg!r}, '
            'data={self.data!r})'
        ).format(self=self)

    def __init__(self, response_type, msg=None, data=None):
        self.response_type = response_type
        self.msg = msg
        self.data = data

    @classmethod
    def error(cls, msg):
        return cls(ResponseType.error, msg=msg)

    @classmethod
    def not_found(cls, msg):
        return cls(ResponseType.not_found, msg=msg)

    @classmethod
    def success(cls, data):
        return cls(ResponseType.success, data=data)

    @property
    def type(self):
        return self.response_type.value

    @property
    def guid(self):
        if isinstance(self.data, SerializedTask):
            return self.data.guid
        try:
            guid = UUID(self.data)
        except ValueError:
            return None
        return str(guid)

    @property
    def status(self):
        if isinstance(self.data, SerializedTask):
            return self.data.status

    @property
    def running(self):
        if isinstance(self.data, SerializedTask):
            return self.data.running

    @property
    def done(self):
        if isinstance(self.data, SerializedTask):
            return self.data.done

    @property
    def pid(self):
        if isinstance(self.data, SerializedTask):
            return self.data.pid

    @property
    def exitcode(self):
        if isinstance(self.data, SerializedTask):
            return self.data.exitcode

    @property
    def result(self):
        if isinstance(self.data, SerializedTask):
            return self.data.result

    @property
    def result_type(self):
        if isinstance(self.data, SerializedTask):
            return self.data.result.result_type.value

    @property
    def result_data(self):
        if isinstance(self.data, SerializedTask):
            return self.data.result.data

    @property
    def task_started(self):
        if isinstance(self.data, SerializedTask):
            return self.data.result.started

    @property
    def task_finished(self):
        if isinstance(self.data, SerializedTask):
            return self.data.result.finished


class SerializedTask:
    __slots__ = (
        'guid', 'status', 'running', 'done', 'pid', 'exitcode', 'result',
    )

    def __repr__(self):
        return (
            'SerializedTask(guid={self.guid}, status={self.status}, '
            'running={self.running}, done={self.done}, pid={self.pid}, '
            'exitcode={self.exitcode}, result={self.result!r})'
        ).format(self=self)

    def __init__(
        self, guid=None, status=None, running=None, done=None, pid=None,
        exitcode=None, result=None,
    ):
        self.guid = guid
        self.status = status
        self.running = running
        self.done = done
        self.pid = pid
        self.exitcode = exitcode
        self.result = result

    @classmethod
    def deserialize(cls, data):
        return cls(**data)
