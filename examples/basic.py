import sys
from time import sleep
from tinytask import Scheduler


def example(*args, **kwargs):
    sleep(3)
    return {
        'success': True,
        'args': args,
        'kwargs': kwargs,
    }


def main():
    sched = Scheduler()
    sched.run()
    print('result dir: {}'.format(sched.RESULT_DIR))

    response = sched.schedule(
        example, args=('foo', 'bar'), kwargs={'fizzbuzz': True},
    )
    # or it could be this format:
    # response = sched.s(example)('foo', 'bar', fizzbuzz=True)

    if response.type == 'success':
        print('guid: {}'.format(response.guid))
    elif response.type == 'error':
        sys.exit('error: {}'.format(response.msg))

    guid = response.guid
    response = None
    while response is None or not response.done:
        response = sched.get(guid)
        print('Got response: {!r}'.format(response))
        if response.type == 'error':
            print('*** Traceback from response ***')
            sys.exit(response.msg)
        sleep(1)
    print('pid: {!r}'.format(response.pid))
    print('exitcode: {!r}'.format(response.exitcode))
    print('status: {!r}'.format(response.status))
    print('result: {!r}'.format(response.result))
    print()
    print('result_type: {!r}'.format(response.result_type))
    print('result_data: {!r}'.format(response.result_data))
    print('task_started: {!r}'.format(response.task_started))
    print('task_finished: {!r}'.format(response.task_finished))
    sched.quit()


if __name__ == '__main__':
    main()
