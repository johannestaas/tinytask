#!/bin/bash

DICE=1
SIDES=20

which jq > /dev/null
if [[ $? != 0 ]] ; then
	echo "requires installation of 'jq'"
	exit 1
fi

if [[ $# == 1 ]] ; then
	SIDES="$1"
elif [[ $# == 2 ]] ; then
	DICE="$1"
	SIDES="$2"
elif [[ $# > 2 ]] ; then
	echo "Usage: ./test_flaskapp.sh 1 20"
	echo "rolls one twenty-sided die"
	exit 1
fi

echo "Rolling ${DICE}d${SIDES}..."

GUID="$(curl -s localhost:5000/roll/$DICE/$SIDES | jq .job | tr -d '\"')"
echo "GUID: $GUID"
while true ; do
	RESULTS="$(curl -s localhost:5000/job/$GUID | jq .results)"
	if [[ "$RESULTS" -eq "null" ]] ; then
		echo -n "."
	else
		echo ""
		echo "Got result: $RESULTS"
		break
	fi
	sleep 1
done
