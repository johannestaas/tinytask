from flask import Flask
from flask_restful import Api, Resource
from tinytask import Scheduler
from time import sleep
from random import randint


app = Flask('example')
api = Api(app)
sched = Scheduler()


def random_number(dice=1, sides=20):
    total = 0
    for i in range(dice):
        total += randint(1, sides)
    sleep(15)
    return total


class JobResource(Resource):

    def get(self, job_guid):
        response = sched.get(job_guid)
        if response.running:
            return {
                'status': response.status,
                'results': None,
            }
        return {
            'status': response.status,
            'results': response.result_data,
        }


class RollResource(Resource):

    def get(self, dice, sides):
        response = sched.s(random_number)(dice=dice, sides=sides)
        return {
            'job': response.guid,
            'status': response.status,
            'roll': '{}d{}'.format(dice, sides),
        }


api.add_resource(JobResource, '/job/<job_guid>')
api.add_resource(RollResource, '/roll/<int:dice>/<int:sides>')

sched.run()
app.run()
sched.quit()
