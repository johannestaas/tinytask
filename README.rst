tinytask
========

A tiny task framework using forked processes without a message broker.

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Check the examples directory.

Release Notes
-------------

:0.1.0:
    Works, scheduler implemented.
:0.0.1:
    Project created
